SPEC
====

**Grid**
---

Argumnets | Default | Min | Max
--------- | ------- | --- | ---
cols      | 25      | 5   | 100
rows      | 25      | 5   | 100

- out
	- API - can be called radiply(most likely AJAX calls, not sure yet?)
		- JSON
	- CLI - probably just some asterisks on the command line such as:

\* \* \* \* \* \* \* \* \* \* \* \*  
\* \* \* \* \* \* \* \* \* \* \* \*  
\* \* \* \* \* \* \* \* \* \* \* \*  
\* \* \* \* \* \* \* \* \* \* \* \*  
\* \* \* \* \* \* \* \* \* \* \* \*  

Argumnets | Options
--------- | -------
out       | API
out       | CLI

- iterations

Argumnets   | Default | Min | Max
----------- | ------- | --- | ---
iternations | 30      | 2   | *

