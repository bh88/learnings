Conway's Game of Life
=========

*Puzzle*: Rules from [**Wikipedia**](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life#Rules)

> The universe of the Game of Life is an infinite two-dimensional orthogonal grid of square cells, each of which is in one of two possible states, alive or dead. Every cell interacts with its eight neighbours, which are the cells that are horizontally, vertically, or diagonally adjacent. At each step in time, the following transitions occur:

> * Any live cell with fewer than two live neighbours dies, as if caused by under-population.
* Any live cell with two or three live neighbours lives on to the next generation.
* Any live cell with more than three live neighbours dies, as if by overcrowding.
* Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.

> The initial pattern constitutes the seed of the system. The first generation is created by applying the above rules simultaneously to every cell in the seed—births and deaths occur simultaneously, and the discrete moment at which this happens is sometimes called a tick (in other words, each generation is a pure function of the preceding one). The rules continue to be applied repeatedly to create further generations.

*Testing*: Patterns from [**Wikipedia**](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life#Examples_of_patterns)

--- 

[**To-Do**](http://english.stackexchange.com/a/46220/7664)

- [ ] Command Line SPEC
- [ ] API SPEC
- [ ] HTML(API) Interface
 
**Languages**

- [ ] C
- [ ] C#
- [ ] C++
- [ ] CoffeeScript
- [ ] D
- [ ] Erlang
- [ ] Go
- [ ] Java
- [ ] JavaScript(Node)
- [ ] JavaScript(HTML)
- [ ] Objective-C
- [ ] Perl
- [ ] PHP
- [ ] Python
- [ ] Ruby
- [ ] Rust
- [ ] Scala
- [ ] Shell
- [ ] Swift