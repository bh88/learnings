var program = require('commander');
var keypress = require('keypress');

var Game = function() {
  // essentially private variables
  var cols = 20;
  var colsMin = 5;
  var colsMax = 100;

  var rows = 55;
  var rowsMin = 5;
  var rowsMax = 100;

  var maxIterations = 1000;
  var currentIteration = 0;

  var aliveChar = '+';
  var deadChar = '-';

  var aliveCount = 0;
  var deadCount = 0;

  var outputType = 'CLI';

  var paused = 0;
  var stopped = 0;

  var timers = [];
  var points = [];
  var generations = [];

  var cli = function() {
    program
      .version('0')
      .usage('[options]')
      .option('-c, --cols <n>', 'Number of columns', parseInt)
      .option('-r, --rows <n>', 'Number of rows', parseInt)
      .option('-i, --iterations <n>', 'Max number of iterations', parseInt)
      .option('-a, --alive-char <s>', 'Character for alive cells')
      .option('-d, --dead-char <s>', 'Character for dead cells')
      .option('-p, --points <arr>', 'Points/Cords for live cells')
      .option('-o, --out <out>', 'Output (CLI|API) [CLI]', 'CLI')
      // .option('--show-detail', 'Show detailed information per col/row')
      .parse(process.argv);

    if(c = program.cols) cols = (c >= colsMin && c <= colsMax) ? c : cols;
    if(r = program.rows) rows = (r >= rowsMin && r <= rowsMax) ? r : rows;

    if(i = program.iterations || program.iterations == 0) {
      // the game of life requires at least 2 turns
      while(i <= 1) i++;
      maxIterations = i;
    }

    if(a = program.aliveChar) aliveChar = a;
    if(d = program.deadChar) deadChar = d;

    points = JSON.parse(program.points) || [];

    // defaults to CLI
    if(-1 !== ['CLI', 'API'].indexOf(program.out.toUpperCase())) {
      outputType = program.out.toUpperCase();
    }
  }

  var controls = function() {
    // make `process.stdin` begin emitting "keypress" events
    keypress(process.stdin);

    // listen for the "keypress" event
    process.stdin.on('keypress', function (ch, key) {
      if(key && key.name == 'p') {
        if(stopped) return;

        clear();
        paused = (paused) ? 0 : 1;
        if(!paused) run();
      }

      if(key && key.name == 'r') {
        // freeze it
        stopped = 1;

        while(timers.length != 0) {
          clearTimeout(timers.shift());
        }

        currentIteration = 0;
        paused = 0;
        stopped = 0;
        run();
      }

      if(key && key.name == 's') {
        stopped = 1;
      }

      if(key && key.name == 'q') {
        clear();
        process.exit();
      }

      if(key && key.ctrl && key.name == 'c') {
        process.exit();
      }
    });

    process.stdin.setRawMode(true);
    process.stdin.resume();
  }

  var clear = function() {
    process.stdout.write('\u001B[J\u001B[0;0f');
  }

  var birth = function(x, y) {
    for(var cords in points) {
      cords = points[cords];

      if(cords.x == x) {
        if(-1 !== cords.y.indexOf(y.toString())) {
          return 1;
        }
      }
    }

    return 0;
  }

  var grid = function() {
    var border = '';
    while(border.length != (cols * 3) + 2) border += '-';

    process.stdout.write('  ' + border + '\n');

    for(var y = rows; y >= 1; y--) {
      var row = '';

      for(var x = 1; x <= cols; x++) {
        if(currentIteration == 0) {
          row += (b = birth(x, y)) ? aliveChar : deadChar;
        } else {
          b = generations[currentIteration - 1][y][x - 1];

          var above = null,
              below = null,
              left = null,
              right = null;

          if(generations[currentIteration - 1][y + 1]) {
            // console.log('a: ', x, y + 1);
            above = generations[currentIteration - 1][y + 1][x -1];
          }

          if(generations[currentIteration - 1][y - 1]) {
            // console.log('b: ', x, y - 1);
            below = generations[currentIteration - 1][y - 1][x - 1];
          }

          if(generations[currentIteration - 1][y][x - 1]) {
            // console.log('l: ', x - 1, y);
            left = generations[currentIteration - 1][y][x - 1];
          }

          if(null !== generations[currentIteration - 1][y][x + 1]) {
            // console.log('r: ', x + 1, y);
            right = generations[currentIteration - 1][y][x + 1];
          }

          // console.log(x, y);
          // console.log(above, below, left, right);

          var livingCells = 0;
          Array.prototype.map.call([above, below, left, right], function(v, i, arr) {
            if(v) livingCells++;
          });

          // console.log(livingCells);

          // Any live cell with fewer than two live
          // neighbours dies, as if caused by under-population.

          if(livingCells < 2) {
            b = 0;
          }

          row += (b) ? aliveChar : deadChar;
        }

        if(x != cols) row += '  ';
        (b) ? aliveCount++ : deadCount++;

        if(!generations[currentIteration]) {
          generations[currentIteration] = [];
        }

        if(!generations[currentIteration][y]) {
          generations[currentIteration][y] = [];
        }

        generations[currentIteration][y].push(b);
      }

      process.stdout.write('  | ' + row + ' |\n');
    }

    process.stdout.write('  ' + border + '\n');
  }

  var out = function() {
    clear();
    process.stdout.write('Conway\'s Game of Life!\n\n');

    aliveCount = 0;
    deadCount = 0;

    grid();

    process.stdout.write('  Iterations: ' + currentIteration);
    process.stdout.write(' | Alive: ' + aliveCount);
    process.stdout.write(' | Dead: ' + deadCount);
    process.stdout.write('\n\n');

    var pAction = (paused) ? '(p)lay' : '(p)ause';
    process.stdout.write('  (q)uit (r)estart (s)top ' + pAction + '\n');
  }

  var run = function() {
    if(paused || stopped || currentIteration == maxIterations) {
      return out();
    }

    out();

    timers.push(setTimeout(function() {
      run();
    }, 1000));

    currentIteration++;
  }

  this.init = function() {
    cli();
    controls();

    clear();
    run();
  }
};

var life = new Game();
life.init();

// TODO
// - webserver/API/CLI
// - rules